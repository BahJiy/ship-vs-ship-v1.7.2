import greenfoot.*;
import java.awt.Color;

/**
 * Quit Button
 * @author Huu Vu
 */
public class Quit extends MenuB
{
    private String text = "Quit";
    private int stringLength = (text.length() + 2) * 10; //Find Text Length
    
    public Quit() 
    {
        setImage(new GreenfootImage(stringLength, 16));
        
        //Create new Greenfoot Image (Text, size (font), Color, Background)
        GreenfootImage image = new GreenfootImage(text, 25, Color.RED, new Color(0, 0, 0, 0));
        
        image.setColor(Color.RED);
        
        setImage(image);
    }
    
    public void act() 
    {
         if (Greenfoot.mouseClicked(this)) Greenfoot.stop(); //Stop the game
    }     
}
