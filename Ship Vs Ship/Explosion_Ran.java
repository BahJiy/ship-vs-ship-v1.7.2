import greenfoot.*;

/**
 * An Exposion that hurt Ships if they are hit
 *
 * Huu Vu
 */
public class Explosion_Ran extends Items{
    //Damage
    private int damage;
    
    // A exp looses one life each act, and will disappear when life = 0
    private int life = 1;
    
    public void act() 
    {
        Rocket ro = (Rocket) getOneIntersectingObject(Rocket.class);
        Rocket2 ro2 = (Rocket2) getOneIntersectingObject(Rocket2.class);
        Asteroid asteroid = (Asteroid) getOneIntersectingObject(Asteroid.class);
        Space space = (Space) getWorld();
        
        int r = Greenfoot.getRandomNumber(20);
        damage = r + 20;

        if (life <= 0) {            
            getWorld().addObject(new Explosion(), getX(), getY());
            getWorld().removeObject(this);            
        } else if (ro != null) {            
            getWorld().removeObject(this);
            space.hit(damage);
            ro.hit(damage);            
        } else if (ro2 != null) {            
            getWorld().removeObject(this);
            space.hit2(damage);
            ro2.hit(damage);            
        } else if (asteroid != null) {
            life = 1;
            asteroid.hit(damage);            
            if (life <= 0) getWorld().removeObject(this);
        } else life--;
    }
}