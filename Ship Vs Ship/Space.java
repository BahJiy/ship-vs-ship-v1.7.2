import greenfoot.*;
import java.awt.Color;
import java.awt.Graphics;
import java.util.List;

/**
 * Space. Something for rockets to fly in...
 *
 * @author Michael Kolling
 * @Edited by Huu Vu
 * @Music by Brendan Ferguson
 */
public class Space extends World {
    /*
     * For Spawner
     */
    private int a = 500;
    private int b = 550;
    private int c = 450;
    private int d = 400;
    private int e = 400;
    private int i = 100;
    
    private MenuB m = new MenuB();
    
    private GreenfootImage background = getBackground(); //Get the current Background
    private int r = Greenfoot.getRandomNumber(10);


    /**
     * Start the Preparation
     */
    public Space() {
        super(1000, 850, 1);
        Explosion.initialiseImages();

        BackG(); //Create a Color BackGround
        new MenuB().start(0);

        prepare();
    }
    
    /**
     * Randomize the Bakcground
     */
    public void BackG() {       
        //If the Background is to be only black then earse the // in next line
        //r == 0;
        if (r <= 2) {
            background.setColor(Color.BLACK);
        } else if (r == 3) {
            background.setColor(Color.DARK_GRAY);
        } else if (r == 4) {
            background.setColor(Color.WHITE);
        } else if (r == 5) {
            background.setColor(Color.BLUE);
        } else if (r == 6) {
            background.setColor(Color.LIGHT_GRAY);
        } else if (r == 7) {
            background.setColor(Color.GREEN);
        } else if (r == 8) {
            background.setColor(Color.YELLOW);
        } else if (r == 9) {
            background.setColor(Color.ORANGE);
        }
        
        background.fill(); //Change the color
        createStars(1000); //Run the Star method and create the stars
    }

    /**
     * Change BackGround For Setting Only!
     */
    public void change(Object option) {
        if (option.equals("Black")) {
            background.setColor(Color.BLACK);
        } else if (option.equals("D Gray")) {
            background.setColor(Color.DARK_GRAY);
        } else if (option.equals("White")) {
            background.setColor(Color.WHITE);
        } else if (option.equals("Blue")) {
            background.setColor(Color.BLUE);
        } else if (option.equals("L Gray")) {
            background.setColor(Color.LIGHT_GRAY);
        } else if (option.equals("Green")) {
            background.setColor(Color.GREEN);
        } else if (option.equals("Yellow")) {
            background.setColor(Color.YELLOW);
        } else if (option.equals("Orange")) {
            background.setColor(Color.ORANGE);
        }

        background.fill();
        createStars(1000);
    }

    /**
     * Create Star in BackGround
     */
    private void createStars(int number) {
        //Create the Stars (the Stars will have different color depending on the Background
        for (int i = 0; i < number; i++) {
            //These value are to place the Star at random locations
            int x = Greenfoot.getRandomNumber(getWidth());
            int y = Greenfoot.getRandomNumber(getHeight());
            
            //Set Star color base on background color
            if (r <= 3 || r == 6) {
                background.setColorAt(x, y, new Color(255, 255, 255));
            } else if (r == 4) {
                background.setColorAt(x, y, new Color(0, 0, 0));
            } else if (r == 5) {
                background.setColorAt(x, y, new Color(255, 53, 51));
            } else if (r == 7) {
                background.setColorAt(x, y, new Color(255, 0, 0));
            } else if (r == 8) {
                background.setColorAt(x, y, new Color(127, 0, 255));
            } else if (r == 9) {
                background.setColorAt(x, y, new Color(0, 0, 255));
            }
        }
    }

    /**
     * Prepare the world for the start of the program. That is: create the
     * initial objects and add them to the world.
     */
    public void prepare() {
        addObject(new Start(), getWidth() / 2, getHeight() / 2); //Adds the text
        addObject(new Quit(), getWidth() / 2, (getHeight() / 2 + 150)); //Adds the text
        addObject(new Instruction(), getWidth() / 2, (getHeight() / 2 + 50)); //Adds the text
        addObject(new Option(), getWidth() / 2, (getHeight() / 2 + 100)); //Adds the text
        addObject(new Menu(), 978, 834); //Menu button
        addObject(new MenuB(), 1, 1); //To as a config file
    }

    /**
     * Counts the score for life
     */
    //For Ship 1
    public void life(int x) {
        List<Counter_Life> score = getObjects(Counter_Life.class); //Call on the Class
        for (Counter_Life s : score) { //
            s.add(x); //Add
        }
    }
    //For Ship 2
    public void life2(int x) {
        List<Counter_Life2> score = getObjects(Counter_Life2.class);
        for (Counter_Life2 s2 : score) {
            s2.add(x);
        }
    }

    /**
     * Counts the score for Health
     */
    //For Ship 1
    public void hit(int x) {
        
    }
    //For Ship 2
    public void hit2(int x) {
        List<Counter_Health2> score = getObjects(Counter_Health2.class);
        for (Counter_Health2 s2 : score) {
            s2.add(-x);
        }
    }

    /**
     * Spawner Code with Reset The RandomNumber are for location
     */
    //Heal
    public void fix() {
        //To Place the Items at random Locations
        int r = Greenfoot.getRandomNumber(getWidth());
        int r2 =  Greenfoot.getRandomNumber(getHeight());
        int r3 = Greenfoot.getRandomNumber(101);

        Fix f = new Fix(); //Create new Object
        addObject(f, r, r2); //Add to the World ("Hello World!")
        a = r3 + 450; //Restart counter
    }
    //Explosion
    public void exp2() {
        int r = Greenfoot.getRandomNumber(getWidth());
        int r2 =  Greenfoot.getRandomNumber(getHeight());
        int r3 = Greenfoot.getRandomNumber(101);

        Greenfoot.playSound("Explosion.mp3");
        Explosion_Ran e = new Explosion_Ran();
        addObject(e, r, r2);
        b = r3 + 500;
    }
    //Life
    public void lifen() {
        int r = Greenfoot.getRandomNumber(getWidth());
        int r2 =  Greenfoot.getRandomNumber(getHeight());
        int r3 = Greenfoot.getRandomNumber(101);

        Life e = new Life();
        addObject(e, r, r2);
        c = r3 + 500;
    }
    //Asteriods
    public void ast() {
        Start s = new Start();
        int r = Greenfoot.getRandomNumber(getWidth());
        int r2 =  Greenfoot.getRandomNumber(getHeight());
        int r3 = Greenfoot.getRandomNumber(151);

        Asteroid e = new Asteroid();
        addObject(e, r, r2);
        if (s.rs() == 0) {
            c = 100;
        } else {
            c = r3 + 500;
        }
    }
    //Turn
    public void turn() {
        int r = Greenfoot.getRandomNumber(getWidth());
        int r2 =  Greenfoot.getRandomNumber(getHeight());
        int r3 = Greenfoot.getRandomNumber(151);

        Turn e = new Turn();
        addObject(e, r, r2);
        d = r3 + 350;
    }
    //Stall
    public void stall() {
        int r = Greenfoot.getRandomNumber(getWidth());
        int r2 =  Greenfoot.getRandomNumber(getHeight());
        int r3 = Greenfoot.getRandomNumber(101);

        Stall d = new Stall();
        addObject(d, r, r2);
        e = r3 + 350;
    }

    public void act() {        
        //Asteriods
        if (c == 0) {
            ast();
        } else {
            c -= 1;
        }

        if (m.rs() == 1) { //Spawn only is user press Start
            //Fix
            if (a == 0) {
                fix();
            } else {
                a -= 1;
            }
            //Explosion
            if (b == 0) {
                exp2();
            } else {
                b -= 1;
            }
            //Turn
            if (d == 0) {
                turn();
            } else {
                d -= 1;
            }
            //Stall
            if (e == 0) {
                stall();
            } else {
                e -= 1;
            }
            //Spawn only if Game Play is NOT Endless
            if (m.re() == 0) { 
                //Life
                if (c == 0) {
                    lifen();
                } else {
                    c -= 1;
                }
            }
        }
    }
}