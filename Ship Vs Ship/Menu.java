import greenfoot.*;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import java.util.List;  

/**
 * Menu Dislog Option
 *
 * @author Huu VU
 */
public class Menu extends Items {
    String temp;

    public void act() {
        if (Greenfoot.mouseClicked(this)) {
            // New Joptionpane Input Dialog with custom list items
            Object option = JOptionPane.showInputDialog(null, "Choose an Option", "Menu",
                    JOptionPane.QUESTION_MESSAGE, null, new Object[]{"Help", "Setting", "Restart", "Quit",
                       "Version"}, "Setting");

             if (option.equals("Help")) this.help();
             else if (option.equals("Quit")) Men(); // Stop/Pause the Game
             else if (option.equals("Restart")) System.exit(0); // Clea Java from the memory
             else if (option.equals("Setting")) this.setting(); // More Setting options
             else if (option.equals("Version")) JOptionPane.showMessageDialog(null, "This is Version 1.7.4"); // Version
        }
    }

    /**
     * Setting Options
     */
    public void setting() {
        Object option = JOptionPane.showInputDialog(null, "Choose an Option", "Debugger",
                JOptionPane.QUESTION_MESSAGE, null, new Object[]{"Change Back. Color", "Play Music?", "Respawn Ship", "Add Ship", "Add Item"}
                , "Change Back. Color");

        if (option.equals("Respawn Ship")) this.respawn();
        else if(option.equals("Add Ship")) this.addShip();
        else if (option.equals("Add Item")) this.item();
        else if (option.equals("Change Back. Color")) this.change();
        else if (option.equals("Play Music?")) this.play();        
    }

    /**
     * Go back to Menu
     */
    public void Men() {
        World w = getWorld();
        
        new MenuB().start(0);

        //Remove Object
        List remove = w.getObjects( Actor.class );
        for (Object objects : remove)
        w.removeObject( ( Actor ) objects );

        w.addObject(new Start(), w.getWidth()/2, w.getHeight()/2); //Adds the text
        w.addObject(new Quit(), w.getWidth()/2, (w.getHeight()/2 + 150)); //Adds the text
        w.addObject(new Instruction(), w.getWidth()/2, (w.getHeight()/2 + 50)); //Adds the text
        w.addObject(new Option(), w.getWidth()/2, (w.getHeight()/2 + 100)); //Adds the text
        w.addObject(new Menu(), 978, 834); //Menu button
        w.addObject(new MenuB(), 1, 1); //To as a config file
    }

    /**
     * Display Help
     */
    public void help() {
        // Create Image to use in Joptionpane
        final ImageIcon icon = new ImageIcon("images/rocket.png");
        final ImageIcon icon2 = new ImageIcon("images/rocket2.png");
        final ImageIcon icon3 = new ImageIcon("images/greenfoot.png");

        JOptionPane.showMessageDialog(null, "This is Player One.\nThe control are:\n"
                + "W = Forward\n"
                + "S = Backward\n"
                + "A = Turn Left\n"
                + "D = Turn Right"
                + "Q = Shoot\n"
                + "E = Bomb", "Help", JOptionPane.INFORMATION_MESSAGE, icon);
                

        JOptionPane.showMessageDialog(null, "This is Player Two.\nThe control are:\n"
                + "Up = Forward\n"
                + "Down = Backward\n"
                + "Left = Turn Left\n"
                + "Right = Turn Right\n"
                + "L Click = Shoot\n"
                + "R Click = Bomb", "Help", JOptionPane.INFORMATION_MESSAGE, icon2);

        JOptionPane.showMessageDialog(null, "Going over this will heal your ship by an X amount (X = 27 - 55)", "Help", JOptionPane.INFORMATION_MESSAGE, icon3);
        JOptionPane.showMessageDialog(null, "For the rest of the appearance, You have to figure it out.", "Help", JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * Respawn Ship
     */
    public void respawn() {
        World w = getWorld();

        int r = Greenfoot.getRandomNumber(1000);
        int r2 = Greenfoot.getRandomNumber(850);

        Object option = JOptionPane.showInputDialog(null, "Choose an Option", "Respawn Ships",
                JOptionPane.QUESTION_MESSAGE, null, new Object[]{"Respawn Ship 1", "Respawn Ship 1 at X & Y",
                    "Respawn Ship 2", "Respawn Ship 2 At X & Y"}, "Respawn Ship 1");

        if (option.equals("Respawn Ship 1")) {
            // Remove the Object from World
            w.removeObjects(w.getObjects(Rocket.class));
            w.removeObjects(w.getObjects(Counter_Health.class));
            w.removeObjects(w.getObjects(Counter_Life.class));

            // Add Object to World
            Rocket rocket = new Rocket();
            w.addObject(rocket, r, r2);
            Counter_Life count = new Counter_Life();
            w.addObject(count, 117, 41);
            Counter_Health heal = new Counter_Health();
            w.addObject(heal, 120, 67);
        } else if (option.equals("Respawn Ship 1 at X & Y")) {
            w.removeObjects(w.getObjects(Rocket.class));
            w.removeObjects(w.getObjects(Counter_Health.class));
            w.removeObjects(w.getObjects(Counter_Life.class));

            // Get the user X and Y coordinates
            temp = JOptionPane.showInputDialog(null, "What is the X Coordinate?", "X Coordinate", 0);
            r = Integer.parseInt(temp);
            temp = JOptionPane.showInputDialog(null, "What is the Y Coordinate?", "Y Coordinate", 1);
            r2 = Integer.parseInt(temp);

            Rocket rocket = new Rocket();
            w.addObject(rocket, r, r2);
            Counter_Life count = new Counter_Life();
            w.addObject(count, 117, 41);
            Counter_Health heal = new Counter_Health();
            w.addObject(heal, 120, 67);
        } if (option.equals("Respawn Ship 2")) {
            w.removeObjects(w.getObjects(Rocket2.class));
            w.removeObjects(w.getObjects(Counter_Health2.class));
            w.removeObjects(w.getObjects(Counter_Life2.class));

            Rocket2 rocket = new Rocket2();
            w.addObject(rocket, r, r2);
            Counter_Life2 count = new Counter_Life2();
            w.addObject(count, 274, 42);
            Counter_Health2 heal = new Counter_Health2();
            w.addObject(heal, 273, 66);
        } else if (option.equals("Respawn Ship 2 at X & Y")) {
            w.removeObjects(w.getObjects(Rocket2.class));
            w.removeObjects(w.getObjects(Counter_Health2.class));
            w.removeObjects(w.getObjects(Counter_Life2.class));

            temp = JOptionPane.showInputDialog(null, "What is the X Coordinate?", "X Coordinate", 0);
            r = Integer.parseInt(temp);
            temp = JOptionPane.showInputDialog(null, "What is the Y Coordinate?", "Y Coordinate", 1);
            r2 = Integer.parseInt(temp);

            Rocket2 rocket = new Rocket2();
            w.addObject(rocket, r, r2);
            Counter_Life2 count = new Counter_Life2();
            w.addObject(count, 274, 42);
            Counter_Health2 heal = new Counter_Health2();
            w.addObject(heal, 273, 66);
        }
    }

    /**
     * Add Ship
     */
    public void addShip() {
        World w = getWorld();

        int r = Greenfoot.getRandomNumber(1000);
        int r2 = Greenfoot.getRandomNumber(850);

        Object option = JOptionPane.showInputDialog(null, "Choose an Option", "Add Ship",
                JOptionPane.QUESTION_MESSAGE, null, new Object[]{"Ship 1", "Ship 1 at X & Y", "Ship 2", "Ship 2 at X & Y"}, "Ship 1");

        if (option.equals("Ship 1")) {
            Rocket rocket = new Rocket();
            w.addObject(rocket, r, r2);
        } else if (option.equals("Ship 1 at X & Y")) {
            temp = JOptionPane.showInputDialog(null, "What is the X Coordinate?", "X Coordinate", 0);
            r = Integer.parseInt(temp);
            temp = JOptionPane.showInputDialog(null, "What is the Y Coordinate?", "Y Coordinate", 1);
            r2 = Integer.parseInt(temp);

            Rocket rocket = new Rocket();
            w.addObject(rocket, r, r2);
        } else if (option.equals("Ship 2")) {
            Rocket2 rocket = new Rocket2();
            w.addObject(rocket, r, r2);
        } else if (option.equals("Ship 2 at X & Y")) {
            temp = JOptionPane.showInputDialog(null, "What is the X Coordinate?", "X Coordinate", 0);
            r = Integer.parseInt(temp);
            temp = JOptionPane.showInputDialog(null, "What is the Y Coordinate?", "Y Coordinate", 1);
            r2 = Integer.parseInt(temp);

            Rocket2 rocket = new Rocket2();
            w.addObject(rocket, r, r2);
        }
    }

    /**
     * Add Item
     */
    public void item() {
        World w = getWorld();

        int r = Greenfoot.getRandomNumber(1000);
        int r2 = Greenfoot.getRandomNumber(850);

        Object option = JOptionPane.showInputDialog(null, "Choose an Option", "Add Item",
                JOptionPane.QUESTION_MESSAGE, null, new Object[]{"Fix", "Life", "Explosion", "Stall", "Turn"}, "Fix");

        if (option.equals("Fix")) {
            Fix e = new Fix();
            w.addObject(e, r, r2);
        } else if (option.equals("Life")) {
            Life e = new Life();
            w.addObject(e, r, r2);
        } else if (option.equals("Explosion")) {
            Explosion_Ran e = new Explosion_Ran();
            w.addObject(e, r, r2);
        } else if (option.equals("Stall")) {
            Stall e = new Stall();
            w.addObject(e, r, r2);
        } else if (option.equals("Turn")) {
            Turn e = new Turn();
            w.addObject(e, r, r2);
        } else if (option.equals("Asteroid")) {
            Asteroid e = new Asteroid();
            w.addObject(e, r, r2);
        }
    }

    /**
     * Change BackGround Color
     */
    public void change() {
        Space space = (Space) getWorld();

        Object option = JOptionPane.showInputDialog(null, "Choose an Option", "Change BackGround Color",
                JOptionPane.QUESTION_MESSAGE, null, new Object[]{"Black", "D Gray", "White", "Blue", "L Gray", "Green", "Yellow", "Orange"}, "Black");

        if (option.equals("Black")) space.change(option);
         else if (option.equals("D Gray")) space.change(option);
         else if (option.equals("White")) space.change(option);
         else if (option.equals("Blue")) space.change(option);
         else if (option.equals("L Gray")) space.change(option);
         else if (option.equals("Green")) space.change(option);
         else if (option.equals("Yellow")) space.change(option);
         else if (option.equals("Orange")) space.change(option);        
    }

    public void play() {
        int x = JOptionPane.showConfirmDialog(null, "Play Music?", "Music", JOptionPane.YES_NO_OPTION);

        MenuB m = new MenuB();

        if (x == 0) m.music(x);
         else if (x == 1) m.music(x);        
    }
}