import greenfoot.*;

/**
 * Increase the number of life Rockets have 
 * 
 * @author Huu Vu
 */
public class Life extends Items
{
    //Life = 0 remove
    private int life = 275;
    
    public void act() {
        //Inersecting with this Object?
        Rocket2 ro2 = (Rocket2) getOneIntersectingObject(Rocket2.class);
        Rocket ro = (Rocket) getOneIntersectingObject(Rocket.class);
        Space space = (Space) getWorld();
        
        //Check if intersecting
        if (life <= 0) getWorld().removeObject(this);            
        else if (ro != null) {            
            ro.life(1); //Add a Life to the counter
            space.life(1); //Add a life to the life counter
            getWorld().removeObject(this); //Remove the object
        } else if (ro2 != null) {            
            ro2.life(1);
            space.life2(1);
            getWorld().removeObject(this);            
        } else life--; //Decrease the life life time counter
    }
}
