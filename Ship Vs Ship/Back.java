import greenfoot.*;
import java.awt.Color;

/**
 * Go Back to the Menu
 * 
 * @author Huu Vu
 */
public class Back extends Option
{
    private String text = "Back";
    private int stringLength = (text.length() + 2) * 10; //Find Text Length
    
    public Back() {             
        setImage(new GreenfootImage(stringLength, 16));
        
        //Create new Greenfoot Image (Text, size (font), Color, Background)
        GreenfootImage image = new GreenfootImage(text, 25, Color.RED, new Color(0, 0, 0, 0));
        
        image.setColor(Color.RED);
        
        setImage(image);   
    }  
    
    public void act() {
        if (Greenfoot.mouseClicked(this)) {
            World w = getWorld();
        
            w.addObject(new Start(), w.getWidth()/2, w.getHeight()/2); //Adds the text
            w.addObject(new Quit(), w.getWidth()/2, (w.getHeight()/2 + 150)); //Adds the text
            w.addObject(new Instruction(), w.getWidth()/2, (w.getHeight()/2 + 50)); //Adds the text
            w.addObject(new Option(), w.getWidth()/2, (w.getHeight()/2 + 100)); //Adds the text
        
            getWorld().removeObject(this);
            w.removeObjects(w.getObjects(Back.class));
            w.removeObjects(w.getObjects(Music.class));
            w.removeObjects(w.getObjects(Players.class));
            w.removeObjects(w.getObjects(Endless.class));
        }
    }
}
