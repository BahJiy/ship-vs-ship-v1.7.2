import greenfoot.*;
import java.awt.*;
//import java.lang.*;
//import javax.swing.JOptionPane;

/**
 * Start Button
 * @author Huu Vu
 */
public class Start extends MenuB {
    private String text = "Start";
    private int stringLength = (text.length() + 2) * 10; //Find Text Length 
    int xx = 0, font = 30; // xx (0 = increase, 1 = decrease) font = font size
    
    
    
    public void act() {
      if (Greenfoot.mouseClicked(this) || Greenfoot.isKeyDown("enter")) prepare();
            
      if (font <= 60 && xx == 0) {font++; // increase the font size to enlarge the text
        if(font == 60) xx = 1;} // when font is at the max size make xx to 1 for decrease
      else if (font != 30 && xx == 1) {font--; // decrease the font size to shrink the text
        if (font == 30) xx = 0;} // when font is at the min make xx to 0 for decrease
        
      setImage(new GreenfootImage(stringLength, 16)); //set GreenfootImage lenght and width

      //Create new Greenfoot Image (Text, size (font), Color, Background)
      GreenfootImage image = new GreenfootImage(text, font, Color.RED, new Color(0, 0, 0, 0));
      setImage(image); //Set the Image
    }    
    
    /**
      * Prepare the world for the start of the program. That is: create the initial objects and add them to the world.
    */
    public void prepare() {
        World w = getWorld();
        MenuB m = new MenuB();

        //Create Random location for Rockets
        int r = Greenfoot.getRandomNumber(950);
        int r2 = Greenfoot.getRandomNumber(800);
        int r3 = Greenfoot.getRandomNumber(950);
        int r4 = Greenfoot.getRandomNumber(800);

        //For Rocket 1
        w.addObject(new Rocket(), r, r2);
        if(m.re() == 0){ // If NOT endless
            w.addObject(new Counter_Life(), 117, 41);
            w.addObject(new Counter_Health(), 120, 67);
        } else w.addObject(new Counter_Health(), 117, 41); // only if Endless
        
        //For Rocket 2
        if (m.rp() == 0) { // If NOT one player
            w.addObject(new Rocket2(), r3, r4);
            if(m.re() == 0){ // if NOT endless
                w.addObject(new Counter_Life2(), 274, 42);
                w.addObject(new Counter_Health2(), 273, 66);
            } else w.addObject(new Counter_Health2(), 274, 42); // only if Endless
        }
        
        // Remove the object from the world
        w.removeObject(this);
        w.removeObjects(w.getObjects(Option.class));
        w.removeObjects(w.getObjects(Quit.class));
        w.removeObjects(w.getObjects(Instruction.class));
        w.removeObjects(w.getObjects(Asteroid.class));
        w.removeObjects(w.getObjects(Life.class));
        w.removeObjects(w.getObjects(Turn.class));
        w.removeObjects(w.getObjects(Stall.class));
        w.removeObjects(w.getObjects(Fix.class));   
        
        m.start(1); // Change xs to 1 (meaning game start)
    }
}
