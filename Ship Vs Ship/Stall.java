import greenfoot.*;

/**
 * Stall the Rockets
 *
 * @Edit by Huu VU
 */

public class Stall extends Items {
    //Life = 0 remove
    private int life = 300;
    
    public void act() {
        Rocket2 ro2 = (Rocket2) getOneIntersectingObject(Rocket2.class);
        Rocket ro = (Rocket) getOneIntersectingObject(Rocket.class);
        Asteroid asteroid = (Asteroid) getOneIntersectingObject(Asteroid.class);
        
        if (life <= 0) getWorld().removeObject(this);
            
         else if (ro != null) {            
            ro.Stall(250);
            getWorld().removeObject(this);            
        } else if (ro2 != null) {            
            ro2.Stall(250);
            getWorld().removeObject(this); 
        } else if (asteroid != null) getWorld().removeObject(this);  
        else life--;
    }
}
