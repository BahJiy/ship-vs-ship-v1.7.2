import greenfoot.*;
import java.awt.Color;
import java.awt.Graphics;

/**
     * Counter for Health of Ship 1
     * @author Huu Vu
 */
public class Counter_Health extends Numbers
{
    private static final Color textColor = new Color(255, 51, 51);

    private int value = 0;
    private int target = 100;
    private String text = "Player 1 Health: ";
    private int stringLength = (text.length() + 2) * 10;

    public Counter_Health()
    {
        setImage(new GreenfootImage(stringLength, 16));
        GreenfootImage image = getImage();
        
        image.setColor(textColor);

        updateImage();
    }
    
    public void act() {
        if(value < target) {
            value++;
            updateImage();
        }
        else if(value > target) {
            value--;
            updateImage();
        }
    }

    public void add(int x)
    {
        target += x;
        if (target <= 0) target = 0;
    }

    /**
     * Make the image
     */
    private void updateImage()
    {
        GreenfootImage image = getImage();
        image.clear();
        image.drawString(text + value, 1, 12);
    }
}
