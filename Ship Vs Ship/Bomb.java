import greenfoot.*;

/**
 * A bullet that can hit asteroids.
 *
 * @author Huu VU
 */
public class Bomb extends Mover {

    //Damage
    private int damage;
    
    // A Bomb looses one life each act, and will disappear when life = 0
    private int life = 18;

    public Bomb(Vector speed, int rotation) {
        super(speed);
        setRotation(rotation);
        increaseSpeed(new Vector(rotation, 15));
        Greenfoot.playSound("EnergyGun.wav");
    }

    public void act() {
        int r = Greenfoot.getRandomNumber(36);
        damage = r + 40;
        
        Rocket2 ro = (Rocket2) getOneIntersectingObject(Rocket2.class);
        Asteroid asteroid = (Asteroid) getOneIntersectingObject(Asteroid.class);
        Explode b = new Explode(getMovement().copy(), getRotation());
        Space space = (Space) getWorld();

        if (life <= 0) {            
            getWorld().addObject(b, getX(), getY());
            getWorld().removeObject(this);            
        } else {            
            move();            
            if (ro != null) {                
                getWorld().removeObject(this);
                space.hit2(damage);
                ro.hit(damage);                
            } else if (asteroid != null) {
                getWorld().addObject(b, getX(), getY());
                getWorld().removeObject(this);
                asteroid.hit(damage);                
            } else life--;
        }
    }
}