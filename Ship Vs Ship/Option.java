import greenfoot.*;
import java.awt.Color;

/**
     * Option Button
     * @author Huu Vu
 */
public class Option extends MenuB {
    private String text = "Option";
    private int stringLength = (text.length() + 2) * 10; //Find Text Length
    MenuB m = new MenuB();
    
    public Option() {       
        setImage(new GreenfootImage(stringLength, 16));
        
        //Create new Greenfoot Image (Text, size (font), Color, Background)
        GreenfootImage image = new GreenfootImage(text, 25, Color.RED, new Color(0, 0, 0, 0));        
        setImage(image);
    }
    
    public void act() {
         if (Greenfoot.mouseClicked(this)) {
             World w = getWorld();
             
             getWorld().removeObject(this);
             w.removeObjects(w.getObjects(Quit.class));
             w.removeObjects(w.getObjects(Start.class));
             w.removeObjects(w.getObjects(Instruction.class));
             
             // w.getWidth() just get the world width
             w.addObject(new Music(), w.getWidth()/2, w.getHeight()/2 ); //Display The Text
             w.addObject(new Players(), w.getWidth()/2, (w.getHeight()/2 + 75) ); //Display The Text
             w.addObject(new Endless(), w.getWidth()/2, (w.getHeight()/2 + 150) ); //Display The Text
             w.addObject(new Back(), 750, 800 ); //Display The Text
         }
    }       
}
