import greenfoot.*;

/**
 * A bullet
 *
 * @author Poul Henriksen 
 * @Edit By Huu Vu
 */
public class Bullet2 extends Mover {

    private int damage;
    
    //A bullet looses one life each act, and will disappear when life = 0
    private int life = 43;

    //Speed and Direction
    public Bullet2(Vector speed, int rotation) {
        super(speed);
        setRotation(rotation);
        increaseSpeed(new Vector(rotation, 15));
        Greenfoot.playSound("EnergyGun.wav");
    }
   
    public void act() {
        int r = Greenfoot.getRandomNumber(41);
        damage = r + 10;
        
        Rocket ro = (Rocket) getOneIntersectingObject(Rocket.class);
        Asteroid asteroid = (Asteroid) getOneIntersectingObject(Asteroid.class);
        Space space = (Space) getWorld();

        if (life <= 0) getWorld().removeObject(this);
        else {         
            move();            
            if (ro != null) {                
                getWorld().removeObject(this);
                space.hit(damage);
                ro.hit(damage);                
            } else if (asteroid != null) {                
                getWorld().removeObject(this);
                asteroid.hit(damage);                
            } else life--;
        }
    }
}