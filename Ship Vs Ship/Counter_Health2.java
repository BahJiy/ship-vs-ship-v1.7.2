import greenfoot.*;
import java.awt.Color;
import java.awt.Graphics;

/**
     * Counter for Health of Ship 2
     * @author Huu Vu
 */
public class Counter_Health2 extends Numbers
{
    private static final Color textColor = new Color(255, 51, 51);
 
    private int value = 0; // The Display value
    private int target = 100; // The target value
    private String text = "Player 2 Health: "; // Text
    private int stringLength = (text.length() + 2) * 10;

    public Counter_Health2()
    {
        setImage(new GreenfootImage(stringLength, 16));
        GreenfootImage image = getImage();
        
        image.setColor(textColor);

        updateImage();
    }
    
    public void act() {
        // Change Value to match target one by increasing or decreasing by 1 then update the image
        // Doing this will cause the text to look like it is changing
        if(value < target) {
            value++;
            updateImage();
        }
        else if(value > target) {
            value--;
            updateImage();
        }
    }

    public void add(int x)
    {
        // Set the target value
        target += x;
        if (target <= 0) target = 0; // If target is less than 0
    }

    /**
     * Make the image
     */
    private void updateImage()
    {
        // Set the image on screen
        GreenfootImage image = getImage();
        image.clear();
        image.drawString(text + value, 1, 12);
    }
}
