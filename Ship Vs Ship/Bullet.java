import greenfoot.*;

/**
 * A bullet
 *
 * @author Poul Henriksen
 * @Edit by Huu VU
 */
public class Bullet extends Mover {

    //Bullet Damage
    private int damage;
    //A bullet looses one life each act, and will disappear when life = 0
    private int life = 43;

    //Speed and Direction
    public Bullet(Vector speed, int rotation) {
        super(speed);
        setRotation(rotation);
        increaseSpeed(new Vector(rotation, 15));
        Greenfoot.playSound("EnergyGun.wav");
    }

    public void act() {
        int r = Greenfoot.getRandomNumber(41); //Random damge number
        damage = r + 10;

        // If it is Interacting with one other object
        Rocket2 ro = (Rocket2) getOneIntersectingObject(Rocket2.class);
        Asteroid asteroid = (Asteroid) getOneIntersectingObject(Asteroid.class);
        Space space = (Space) getWorld();

        // Check if object is being interacted with
        if (life <= 0) getWorld().removeObject(this);
        else {
            move();
            if (ro != null) {
                getWorld().removeObject(this);
                space.hit2(damage);
                ro.hit(damage);
            } else if (asteroid != null) {
                getWorld().removeObject(this);
                asteroid.hit(damage);
            } else life--;
        }
    }
}