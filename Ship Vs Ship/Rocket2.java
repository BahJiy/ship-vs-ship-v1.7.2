import greenfoot.*;

/**
 * A rocket that can be controlled by Up, Down, Left, Right Fire buttons are Left Click for bullets
 * and Right Click for bomb
 *
 * @author Poul Henriksen
 * @author Michael Kolling
 * @Edit Huu VU
 */
public class Rocket2 extends Mover {

    private int gunReloadTime = 100;                        // The minimum delay between firing the gun.
    private int reloadDelayCount = 0;                       // How long ago we fired the gun the last time.
    private int bombReload = 250;                           // Reload for Bomb
    private int doublefire = 250;                           // Shot two bullet
    private int shotsFired = 0;                             // Number of shots fired.
    int stability = 100;                                    // Life For Rocket
    int lifen = 0;                                          // Amount of Life
    int action = 0;                                         // Check Keys?
    int check = 0;                                          // Check Speed?
    private Vector acceleration = new Vector(0, 0.1);       // To Move Forward
    private Vector deceleration = new Vector(0, -0.3);      // To Move Backward 
    private Vector movement;                                // The Ship moving
    int Do = 0;                                             // Cause Mode to Change
    private MenuB m = new MenuB();                          // Refer to MenuB
    
    private GreenfootImage rocket = new GreenfootImage("rocket2.png");
    private GreenfootImage rocketWithThrust = new GreenfootImage("rocketWithThrust2.png");

    /**
     * Initilise this rocket.
     */
    public Rocket2() {
        increaseSpeed(new Vector(13, 0.3)); // initially slowly drifting
    }

    /**
     * Return the current stability of this ship. (If it goes down to zero, it
     * breaks up.)
     */
    public int getStability() {
        return stability;
    }

    /**
     * Return the current number of life of this ship.
     */
    public int getLife() {
        return lifen;
    }

    /**
     * Return the number of shots fired from this rocket.
     */
    public int getShotsFired() {
        return shotsFired;
    }

    /**
     * Return the number of time before stall is finish
     */
    public int getStall() {
        return action;
    }

    /**
     * Return the approximate current travelling speed of this rocket.
     */
    public int getSpeed() {
        return (int) (getMovement().getLength() * 10);
    }
    
    /**
     * Return the Direction of the Ship
     */
    public int getDir() {
        return getMovement().getDirection();
    }

    /**
     * Do what a rocket's gotta do.
     */
    public void act() {
        move(); // Move the rocket
        reloadDelayCount++; // Add 1 to the reload count (so the rocket can shoot)
        if(getStability() <= 0) Explode(); //If manually damage the ship
        if (check == 0) { // If check is not 0 ship is unlimited (getting random turn allow for a short time)
            checkSpeed();
        } else check--;
        if (action == 0) { // If action is not 0 ship is disable (can't move) (getting stall does this)
            checkKeys();
        } else {
            setImage(rocket);
            action--;
        }
    }

    /**
     * Make Rocket Turn Randomly
     */
    public void randturn() {
        int r = Greenfoot.getRandomNumber(5) + 1;
        int r2 = Greenfoot.getRandomNumber(360) - 180;

        getMovement().setLength((getMovement().getLength() + r)); // Increase the speed
        getMovement().setDirection(r2); // Random direction
    }

    /**
     * Change the Mode
     */
    public void Change(int add) {
        Do = Do + add;

        if (Do >= 1) {
            bombReload = 350;
            gunReloadTime = 200;
        }
    }

    /**
     * Make the Rocket Stall
     */
    public void Stall(int x) {
        action = action + x; // Add value to action
    }
    
    /**
     * Make the Rocket Keep Speed
     */
    public void noSpeed(int x) {
        check = check + x; // Add value to check
    }

    /**
     * Add life
     */
    public void life(int lifes) {
        lifen = lifen + lifes; // Add value to lifes
    }

    /**
     * Hit this ship dealing the given amount of damage.
     */
    public void hit(int damage) {
        stability = stability - damage; // Add value to ship health

        if (stability <= 0) Explode(); // if Health is 0 explode
    }

    /**
     * BlowUp
     */
    private void Explode() {
        World w = getWorld();
        
        GreenfootSound sound = new GreenfootSound("MetalExplosion.wav"); // Play sound
        sound.setVolume(70); // Set volume
        sound.play(); // Play
        w.addObject(new Explosion_Ran(), getX(), getY()); // create a explosion

        if(lifen < 0 || m.re() == 1) respawn();
        else w.removeObject(this);
    }
    /** Respawn */
    public void respawn() {
        int r = Greenfoot.getRandomNumber(1000);
        int r2 = Greenfoot.getRandomNumber(850);
        World w = getWorld();
    
        w.removeObjects(w.getObjects(Counter_Health2.class)); // Remove the Counter
    
        w.removeObject(this); // Remove the rocket
    
        if(m.re() == 1) {
            w.addObject(new Counter_Health2(), 274, 42);
            w.addObject(new Rocket2(), r, r2);
        } else {
            w.addObject(new Counter_Health2(), 273, 66);
            w.addObject(new Rocket2(), r, r2);
            Rocket ro = new Rocket();
            ro.Change(1);
        }
    
    }

    /**
     * Check whether there are any key pressed and react to them.
     */
    private void checkKeys() {      
        MouseInfo m = Greenfoot.getMouseInfo();  
        
        if (Greenfoot.isKeyDown("up")) {
            setImage(rocketWithThrust);
            acceleration.setDirection(getRotation());
            increaseSpeed(acceleration);
        } else if (Greenfoot.isKeyDown("down")) {
            setImage(rocketWithThrust);
            deceleration.setDirection(getRotation());
            increaseSpeed(deceleration);
        } else setImage(rocket);
        if (Greenfoot.isKeyDown("left")) setRotation(getRotation() - 5);
        if (Greenfoot.isKeyDown("right")) setRotation(getRotation() + 5);
        //Left Click
        if (Greenfoot.mouseClicked(null) && m.getButton() == 1) fire();
        //Right Click
        if (Greenfoot.mouseClicked(null) && m.getButton() == 3) firebomb();
    }
    /**
     * Slow the Rocket?
     */
    private void checkSpeed() {
        if(getSpeed() > 250)
        getMovement().setLength(25);
    }

    /**
     * Fire a bullet if the gun is ready.
     */
    private void fire() {
        int r = Greenfoot.getRandomNumber(30);

        if (reloadDelayCount >= doublefire) { // If reload = with the double shot (longer then normal)
            Bullet2 b = new Bullet2(getMovement().copy(), getRotation()); // New Bullet with rocket vector
            Bullet2 c = new Bullet2(getMovement().copy(), getRotation() - r);

            getWorld().addObject(b, getX(), getY()); // Add the object
            getWorld().addObject(c, getX(), getY());

            b.move(); // Move the bullet
            c.move();

            shotsFired += 2; // Add to shoot counter
            reloadDelayCount = 0; // Reset the reload counter
        } else if (reloadDelayCount >= gunReloadTime) {
            Bullet2 b = new Bullet2(getMovement().copy(), getRotation());
            getWorld().addObject(b, getX(), getY());
            b.move();
            shotsFired++;
            reloadDelayCount = 0;
        }
    }

    /**
     * Fire a bomb if the gun is ready.
     */
    private void firebomb() {
        if (reloadDelayCount >= bombReload) {
            Bomb2 b = new Bomb2(getMovement().copy(), getRotation());
            getWorld().addObject(b, getX(), getY());
            b.move();
            shotsFired++;
            reloadDelayCount = 0;
        }
    }
}