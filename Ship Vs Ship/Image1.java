import greenfoot.*;

/**
 * Intruction
 * @author Huu Vu
 */
public class Image1 extends For1
{
    private GreenfootImage rocket = new GreenfootImage("rocket.png");
    private GreenfootImage rocketWithThrust = new GreenfootImage("rocketWithThrust.png");
    
    /**
     * Check whether there are any key pressed and react to them.
     */
    public void act() {
        //Just to allow the user to turn and see the ship booster on or off
        //Doesn't move the ship!
        if (Greenfoot.isKeyDown("w")) setImage(rocketWithThrust);            
        else if (Greenfoot.isKeyDown("s")) setImage(rocketWithThrust);            
        else setImage(rocket);
        if (Greenfoot.isKeyDown("a")) setRotation(getRotation() - 5);
        if (Greenfoot.isKeyDown("d")) setRotation(getRotation() + 5);
    }       
}
