import greenfoot.*;  
public class MenuB extends Actor {
    // static variable don't change when game is Reset
    static int xp = 0, xe = 0, xm = 1, xs = 0;
    
    //The class below are use to change and return the value of variable that are use for configuration
    public void start(int x) {
        xs = x;
    }

    public int rs() {
        return xs;
    }

    public void play(int x) {
        xp = x;
    }

    public int rp() {
        return xp;
    }

    public void end(int x) {
        xe = x;
    }

    public int re() {
        return xe;
    }

    public void music(int x) {
        xm = x;
    }

    public int rm() {
        return xm;
    }
    
    /**
     * For Music
     */
    int r2 = Greenfoot.getRandomNumber(2); // Use to Random the the music to be play first
    GreenfootSound song1 = new GreenfootSound("s1.mp3"); // Define the music file
    GreenfootSound song2 = new GreenfootSound("s2.mp3");

    public void act() {
        if (xm == 0) { // Allow Music
            if (!song1.isPlaying() & !song2.isPlaying()) { // Run only if music is NOT playing
                if (r2 == 0) { // if r2  == 0
                    r2++; // Add 1 to r2
                    song1.play(); // Play first song
                } else if (r2 == 1) { //If r2 == 1
                    r2--; // Minus 1
                    song2.play(); // Play second song
                }
            }
        } else { // If music should NOT be playing then stop all
            song1.stop();
            song2.stop();
        }
    }
}
