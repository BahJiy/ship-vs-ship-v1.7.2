import greenfoot.*;

/**
 * Healing Power
 *
 * @Edit by Huu VU
 */

public class Turn extends Items {

    //Life = 0 remove
    private int life = 300;
    
    public void act() {
        Rocket2 ro2 = (Rocket2) getOneIntersectingObject(Rocket2.class);
        Rocket ro = (Rocket) getOneIntersectingObject(Rocket.class);
        Asteroid asteroid = (Asteroid) getOneIntersectingObject(Asteroid.class);
    
        if (life <= 0) getWorld().removeObject(this);            
         else if (ro != null) {            
            ro.randturn();
            ro.noSpeed(200);
            getWorld().removeObject(this);            
        } else if (ro2 != null) {            
            ro2.randturn();
            ro2.noSpeed(200);
            getWorld().removeObject(this);            
        } else if (asteroid != null) {            
            asteroid.randturn();
            getWorld().removeObject(this);            
        } else life--;
    }
}
