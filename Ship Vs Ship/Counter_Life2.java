import greenfoot.*; 
import java.awt.Color;
import java.awt.Graphics;

/**
     * Counter for life of Ship 2
     * @author Huu Vu
 */
public class Counter_Life2 extends Numbers
{
    private static final Color textColor = new Color(255, 51, 51);
    
    private int value = 0;
    private int target = 0;
    private String text = "Player 2 Life: ";
    private int stringLength = (text.length() + 2) * 10;

    public Counter_Life2()
    {
        setImage(new GreenfootImage(stringLength, 16));
        GreenfootImage image = getImage();
        
        image.setColor(textColor);

        updateImage();
    }
    
    public void act() {
        if(value < target) {
            value++;
            updateImage();
        }
        else if(value > target) {
            value--;
            updateImage();
        }
    }

    public void add(int x)
    {
        target += x;
        if (target <= 0) target = 0;
    }

    public int getValue()
    {
        return value;
    }

    /**
     * Make the image
     */
    private void updateImage()
    {
        GreenfootImage image = getImage();
        image.clear();
        image.drawString(text + value, 1, 12);
    }
}
