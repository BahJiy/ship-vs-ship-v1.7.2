import greenfoot.*;

/**
 * A rocket that can be controlled by up, down, left, right Fire buttons are
 * Control for bullets and Shift for bomb
 *
 * @author Poul Henriksen
 * @author Michael Kolling
 * @Edit Huu VU
 */
public class Rocket extends Mover {

    private int gunReloadTime = 100;                        // The minimum delay between firing the gun.
    private int reloadDelayCount = 0;                       // How long ago we fired the gun the last time.
    private int bombReload = 250;                           // Reload for Bomb
    private int doublefire = 250;                           // Shot two bullet
    private int shotsFired = 0;                             // Number of shots fired
    int stability = 100;                                    // Life For Rocket
    int lifen = 0;                                          // Amount of Life
    int action = 0;                                         // Check Keys?
    int check = 0;                                          // Check Speed?
    private Vector acceleration = new Vector(0, 0.1);       // To Move Forward
    private Vector deceleration = new Vector(0, -0.3);      // To Move Backward 
    private Vector movement;                                // The Ship moving
    int Do = 0;                                             // Cause Mode to Change
    private MenuB m = new MenuB();                          // Refer to MenuB
    
    private GreenfootImage rocket = new GreenfootImage("rocket.png");
    private GreenfootImage rocketWithThrust = new GreenfootImage("rocketWithThrust.png");

    /**
     * Initilise this rocket.
     */
    public Rocket() {
        increaseSpeed(new Vector(13, 0.3)); // Initially slowly drifting
    }

    /**
     * Return the current stability of this ship. (If it goes down to zero, it
     * breaks up.)
     */
    public int getStability() {
        return stability;
    }

    /**
     * Return the current number of life of this ship.
     */
    public int getLife() {
        return lifen;
    }

    /**
     * Return the number of shots fired from this rocket.
     */
    public int getShotsFired() {
        return shotsFired;
    }

    /**
     * Return the number of time before stall is finish
     */
    public int getStall() {
        return action;
    }

    /**
     * Return the approximate current travelling speed of this rocket.
     */
    public int getSpeed() {
        return (int) (getMovement().getLength() * 10);
    }
    
    /**
     * Return the Direction of the Ship
     */
    public int getDir() {
        return getMovement().getDirection();
    }
    
    /**
     * Return the X of the Ship
     */
    public int getx() {
        return getX();
    }
    /**
     * Return the Y of the Ship
     */
    public int gety() {
        return getY();
    }

    /**
     * Do what a rocket's gotta do.
     */
    public void act() {
        move();
        reloadDelayCount++;
        if(getStability() <= 0) Explode(); //If manually damage the ship
        if (check == 0) {
            checkSpeed();
        } else check--;
        if (action == 0) {
            checkKeys();
        } else {
            setImage(rocket);
            action--;
        }
    }

    /**
     * Make Rocket Turn Randomly
     */
    public void randturn() {
        int r = Greenfoot.getRandomNumber(5) + 1;
        int r2 = Greenfoot.getRandomNumber(360) - 180;

        getMovement().setLength((getMovement().getLength() + r));
        getMovement().setDirection(r2);
    }

    /**
     * Change the Mode
     */
    public void Change(int add) {
        Do = Do + add;

        if (Do >= 1) {
            bombReload = 1;
            gunReloadTime = 1;
        }
    }

    /**
     * Make the Rocket Stall
     */
    public void Stall(int stall) {
        action = action + stall;
    }
    
    /**
     * Make the Rocket Keep Speed
     */
    public void noSpeed(int x) {
        check = check + x;
    }

    /**
     * Add life
     */
    public void life(int lifes) {
        lifen = lifen + lifes;
    }

    /**
     * Hit this ship dealing the given amount of damage.
     */
    public void hit(int damage) {
        stability = stability - damage;

        if (stability <= 0) Explode();
    }

    /**
     * BlowUp
     */
    private void Explode() {
        World w = getWorld();
        
        GreenfootSound sound = new GreenfootSound("MetalExplosion.wav");
        sound.setVolume(70);
        sound.play();
        w.addObject(new Explosion_Ran(), getX(), getY());

        if(lifen < 0 || m.re() == 1) respawn();
        else w.removeObject(this);
    }
    /** Respawn */
    public void respawn() {
        int r = Greenfoot.getRandomNumber(1000);
        int r2 = Greenfoot.getRandomNumber(850);
        World w = getWorld();
    
        w.removeObjects(w.getObjects(Counter_Health.class));
        w.removeObject(this);
    
        if(m.re() == 1) {
            w.addObject(new Counter_Health(), 117, 41);
            w.addObject(new Rocket(), r, r2);
        } else {
            w.addObject(new Counter_Health(), 120, 67);
            w.addObject(new Rocket(), r, r2);
            Rocket2 ro = new Rocket2();
            ro.Change(1);
        }
    }

    /**
     * Check whether there are any key pressed and react to them.
     */
    public void checkKeys() {
       String x = "w";
       if (Greenfoot.isKeyDown(x)) {
            setImage(rocketWithThrust);
            acceleration.setDirection(getRotation());
            increaseSpeed(acceleration);
        } else if (Greenfoot.isKeyDown("s")) {
            setImage(rocketWithThrust);
            deceleration.setDirection(getRotation());
            increaseSpeed(deceleration);
        } else setImage(rocket);
       if (Greenfoot.isKeyDown("a")) setRotation(getRotation() - 5);
       if (Greenfoot.isKeyDown("d")) setRotation(getRotation() + 5);
       if (Greenfoot.isKeyDown("q")) fire();
       if (Greenfoot.isKeyDown("e")) firebomb();
    }
    /**
     * Slow the Rocket?
     */
    private void checkSpeed() {
        if(getSpeed() > 250)
        getMovement().setLength(25);
    }

    /**
     * Fire a bullet if the gun is ready.
     */
    private void fire() {
        int r = Greenfoot.getRandomNumber(30);

        if (reloadDelayCount >= doublefire) {
            Bullet b = new Bullet(getMovement().copy(), getRotation());
            Bullet c = new Bullet(getMovement().copy(), getRotation() - r);

            getWorld().addObject(b, getX(), getY());
            getWorld().addObject(c, getX(), getY());

            b.move();
            c.move();

            shotsFired += 2;
            reloadDelayCount = 0;
        } else if (reloadDelayCount >= gunReloadTime) {
            Bullet b = new Bullet(getMovement().copy(), getRotation());
            getWorld().addObject(b, getX(), getY());
            b.move();
            shotsFired++;
            reloadDelayCount = 0;
        }
    }

    /**
     * Fire a bomb if the gun is ready.
     */
    private void firebomb() {
        if (reloadDelayCount >= bombReload) {

            Bomb b = new Bomb(getMovement().copy(), getRotation());
            getWorld().addObject(b, getX(), getY());
            b.move();
            shotsFired++;
            reloadDelayCount = 0;

        }
    }
}