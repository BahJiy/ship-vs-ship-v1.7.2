import greenfoot.*;
import java.awt.Color;

/**
 * Player Button
 * @author Huu Vu
 */
public class Endless extends Option
{
    private String text; //Text;
    private int stringLength;
    int x = 0;
    
    public Endless() {
        if (m.re() == 0) text = "Endless Games Play?       No";
        else if (m.re() == 1) text = "Endless Games Play?       Yes";
        X();  
    } 
        
    public void X() {
        stringLength = (text.length() + 2) * 10; //Find Text Length

        setImage(new GreenfootImage(stringLength, 16));
        
        //Create new Greenfoot Image (Text, size (font), Color, Background)
        GreenfootImage image = new GreenfootImage(text, 25, Color.RED, new Color(0, 0, 0, 0));
        
        image.setColor(Color.RED);
        
        setImage(image); 
    }
    
    public void act() 
    {
        if (Greenfoot.mouseClicked(this) && text == "Endless Games Play?       Yes") {
            text = "Endless Games Play?       No"; //Text;
            X();  m.end(0);
        } else if (Greenfoot.mouseClicked(this) && text == "Endless Games Play?       No") {
            text = "Endless Games Play?       Yes"; //Text;
            X(); m.end(1);
        }
    }
}
