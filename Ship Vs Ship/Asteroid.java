import greenfoot.*;  // (World, Actor, GreenfootImage, and Greenfoot)

/**
 * A rock in space.
 *
 * @author Poul Henriksen
 * @author Michael Kolling
 * @Edit Huu Vu
 */
public class Asteroid extends Mover {

    /**
     * Size of this asteroid
     */
    private int size;
    
    /**
     * When the stability reaches 0 the asteroid will explode
     */
    private int stability;
    
    //Damage
    private int damage;
    
    //Stop Turn
    int a = 0;
    
    MenuB m = new MenuB();; //Click And Destory?

    /**
     * Create an asteroid with default size and speed.
     */
    public Asteroid() {
        this(50);
    }

    /**
     * Create an asteroid with a given size and default speed.
     */
    public Asteroid(int size) {
        this(size, new Vector(Greenfoot.getRandomNumber(360), 2));
    }

    /**
     * Create an asteroid with a given size size and speed.
     */
    private Asteroid(int size, Vector speed) {
        super(speed);
        setSize(size);
    }

    /**
     * Let the asteroid act. That is: fly around.
     */
    public void act() {
        move();

        //int r = Greenfoot.getRandomNumber(20);
        damage = stability;

        Rocket ro = (Rocket) getOneIntersectingObject(Rocket.class);
        Rocket2 ro2 = (Rocket2) getOneIntersectingObject(Rocket2.class);
        Asteroid asteroid = (Asteroid) getOneIntersectingObject(Asteroid.class);
        Space space = (Space) getWorld();

        //If Asteroid hit another object
        if (ro != null) {
            space.hit(damage);
            ro.hit(damage);            
            explode();
        } else if (ro2 != null) {
            space.hit2(damage);
            ro2.hit(damage);            
            explode();
        } else if (asteroid != null) {
            explode();
        }

        if(m.rs() == 0 && Greenfoot.mouseClicked(this)) explode(); //Allow Click and Destroy
    }

    /**
     * Set the size of this asteroid. Note that stability is directly related to
     * size. Smaller asteroids are less stable.
     */
    public void setSize(int size) {
        stability = size;
        this.size = size;
        GreenfootImage image = getImage();
        image.scale(size, size);
    }

    /**
     * Return the current stability of this asteroid. (If it goes down to zero,
     * it breaks up.)
     */
    public int getStability() {
        return stability;
    }

    /**
     * Hit this asteroid dealing the given amount of damage.
     */
    public void hit(int damage) {
        stability = stability - damage;
        if (stability <= 0) {
            explode();
        }
    }

    /**
     * Healing
     */
    public void heal(int number) {
        stability = stability + number;
    }

    /**
     * Make Asteroid Turn Randomly
     */
    public void randturn() {
        int r = Greenfoot.getRandomNumber(5) + 1;
        int r2 = Greenfoot.getRandomNumber(360) - 180;

        getMovement().setLength((getMovement().getLength() + r));
        getMovement().setDirection(r2);
    }

    /**
     * Explodes this asteroid into two smaller asteroids
     */
    private void explode() {
        GreenfootSound sound = new GreenfootSound("Explosion.wav");
        sound.setVolume(70);

        if (size <= 10) {
            getWorld().removeObject(this);
            return;
        } else {
            int r = getMovement().getDirection() + Greenfoot.getRandomNumber(45);
            double l = getMovement().getLength();
            Vector speed1 = new Vector(r + 60, l * 1.2);
            Vector speed2 = new Vector(r - 60, l * 1.2);
            Vector speed3 = new Vector(r - 60, l * 1.2);
            Asteroid a1 = new Asteroid(size / 2, speed1);
            Asteroid a2 = new Asteroid(size / 2, speed2);
            Asteroid a3 = new Asteroid(size / 2, speed2);
            getWorld().addObject(a1, getX(), getY());
            getWorld().addObject(a2, getX(), getY());
            getWorld().addObject(a3, getX(), getY());
            a1.move();
            a2.move();
            a3.move();

            getWorld().removeObject(this);
        }
    }
}
