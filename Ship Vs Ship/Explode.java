import greenfoot.*;

/**
 * A bullet that can hit asteroids.
 *
 * @author Huu VU
 */
public class Explode extends Mover {

    //Damage
    private int damage;

    // explode2 looses one life each act, and will disappear when life = 0
    private int life = 3;

    public Explode(Vector speed, int rotation) {
        super(speed);
        setRotation(rotation);
        increaseSpeed(new Vector(rotation, 15));
        GreenfootSound sound = new GreenfootSound("Explosion.wav");
        sound.setVolume(70);
        sound.play();
    }

    public void act() {
        int r = Greenfoot.getRandomNumber(31);
        damage = r + 70;
        
        Rocket2 ro = (Rocket2) getOneIntersectingObject(Rocket2.class);
        Asteroid asteroid = (Asteroid) getOneIntersectingObject(Asteroid.class);
        Space space = (Space) getWorld();
        
        if (life <= 0) getWorld().removeObject(this);
            
        else {            
            if (ro != null) {                
                getWorld().removeObject(this);
                space.hit2(damage);
                ro.hit(damage);                
            } else if (asteroid != null) {                
                life = 1;                
                if (life <= 0) {                    
                    life--;
                    getWorld().removeObject(this);                    
                }                
                asteroid.hit(damage);                
            } else life--;
        }
    }
}