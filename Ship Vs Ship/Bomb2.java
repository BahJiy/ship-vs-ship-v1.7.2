import greenfoot.*;

/**
 * A bomb
 *
 * @author Huu Vu
 */
public class Bomb2 extends Mover {

    //Damage of Bomb
    private int damage;
    
    //A bullet looses one life each act, and will disappear when life = 0
    private int life = 18;

    public Bomb2(Vector speed, int rotation) {
        super(speed);
        setRotation(rotation);
        increaseSpeed(new Vector(rotation, 15));
        Greenfoot.playSound("EnergyGun.wav");
    }

    public void act() {
        int r = Greenfoot.getRandomNumber(36);
        damage = r + 40;
        
        Rocket ro = (Rocket) getOneIntersectingObject(Rocket.class);
        Asteroid asteroid = (Asteroid) getOneIntersectingObject(Asteroid.class);
        Explode2 b = new Explode2(getMovement().copy(), getRotation());
        Space space = (Space) getWorld();

        if (life <= 0) {            
            getWorld().addObject(b, getX(), getY());
            getWorld().removeObject(this);            
        } else {            
            move();            
            if (ro != null) {                
                getWorld().removeObject(this);
                space.hit(damage);
                ro.hit(damage);                
            } else if (asteroid != null) {                
                getWorld().addObject(b, getX(), getY());
                getWorld().removeObject(this);
                asteroid.hit(damage);                
            } else life--;
        }
    }
}