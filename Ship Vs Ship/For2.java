import greenfoot.*;
import java.awt.Color;

/**
 * Instruction for Rocket 2
 * @author Huu Vu
 */
public class For2 extends Instruction
{
    private String text;
    private int stringLength;
    
    public For2() {
        text = "This is Player Two.\nThe control are:\n\n" +
                "Up = Forward\n"
                + "Down = Backward\n"
                + "Left = Turn Left\n"
                + "Right = Turn Right\n"
                + "L Click = Shoot\n"
                + "R Click = Bomb"; //Text
        
        stringLength = (text.length() + 2) * 10; //Find Text Length

        setImage(new GreenfootImage(stringLength, 16));
        
        //Create new Greenfoot Image (Text, size (font), Color, Background)
        GreenfootImage image = new GreenfootImage(text, 25, Color.RED, new Color(0, 0, 0, 0));
        
        image.setColor(Color.RED);
        
        setImage(image);   
    }
}