import greenfoot.*;
import java.awt.Color;

/**
 * Intruction Button
 * @author Huu Vu
 */
public class Instruction extends MenuB {
    private String text = "Instruction";
    private int stringLength = (text.length() + 2) * 10; //Find Text Length
    
    public Instruction() 
    {
        set();
    }
    
    public void act() 
    {
         if (Greenfoot.mouseClicked(this)) instruct();
    }    
    
    public void set() {    
        setImage(new GreenfootImage(stringLength, 16));
        
        //Create new Greenfoot Image (Text, size (font), Color, Background)
        GreenfootImage image = new GreenfootImage(text, 25, Color.RED, new Color(0, 0, 0, 0));
        
        image.setColor(Color.RED);
        
        setImage(image);
    }   
    
    public void instruct() {
        World w = getWorld();
        w.addObject(new For1(), w.getWidth()/2, w.getHeight()/2 ); //Display The Text
        w.addObject(new Image1(), (w.getWidth()/2 - 150), w.getHeight()/2 ); //Display The Text
        w.addObject(new next1(), 750, 800 ); //Display The Text
        
        getWorld().removeObject(this);
        w.removeObjects(w.getObjects(Quit.class));
        w.removeObjects(w.getObjects(Start.class));
        w.removeObjects(w.getObjects(Option.class));
    }  
}
