import greenfoot.*;
import java.awt.Color;

/**
     * Player Button
     * @author Huu Vu
 */
public class Players extends Option
{
    private String text; //Text;
    private int stringLength;
    int x = 0;
    
    public Players()
    {
        // What is the value of the require variable and set the text correctly
        if (m.rp() == 0) text  = "One Player?       No";
        if (m.rp() == 1) text = "One Player?       Yes";
        X();  
    } 
    
    public void X() {
        stringLength = (text.length() + 2) * 10; //Find Text Length

        setImage(new GreenfootImage(stringLength, 16));
        
        //Create new Greenfoot Image (Text, size (font), Color, Background)
        GreenfootImage image = new GreenfootImage(text, 25, Color.RED, new Color(0, 0, 0, 0));
        
        image.setColor(Color.RED);
        
        setImage(image); 
    }
    
    public void act() {
        if (Greenfoot.mouseClicked(this) && text == "One Player?       Yes") {
            text = "One Player?       No"; //Text;
            X(); m.play(0); 
        } else if (Greenfoot.mouseClicked(this) && text == "One Player?       No") {
            text = "One Player?       Yes"; //Text;
            X(); m.play(1); 
        }
    }
}
