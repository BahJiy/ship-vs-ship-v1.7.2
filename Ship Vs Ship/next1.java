import greenfoot.*;
import java.awt.Color;

/**
 * Intruction
 * @author Huu Vu
 */
public class next1 extends For1
{
    private String text = "Next";
    private int stringLength = (text.length() + 2) * 10; //Find Text Length
    
    public next1() {      
        setImage(new GreenfootImage(stringLength, 16));
        
        //Create new Greenfoot Image (Text, size (font), Color, Background)
        GreenfootImage image = new GreenfootImage(text, 25, Color.RED, new Color(0, 0, 0, 0));
        
        image.setColor(Color.RED);
        
        setImage(image);   
    }
    
    public void act()
    {
        if (Greenfoot.mouseClicked(this) || Greenfoot.isKeyDown("enter"))
        {
            World w = getWorld();
            getWorld().removeObject(this);
            w.removeObjects(w.getObjects(Image1.class));
            w.removeObjects(w.getObjects(For1.class));
            
            w.addObject(new For2(), w.getWidth()/2, w.getHeight()/2 ); //Display The Text
            w.addObject(new Image2(), (w.getWidth()/2 - 150), w.getHeight()/2 ); //Display The Text
            w.addObject(new next2(), 750, 800 ); //Display The Text
        }
    }
}
