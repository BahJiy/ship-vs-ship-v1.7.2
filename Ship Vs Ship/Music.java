import greenfoot.*;
import java.awt.Color;

/**
     * Music Button
     * @author Huu Vu
 */
public class Music extends Option {
    private String text; //Text;
    private int stringLength;
    int x = 0;
    
    public Music() {
        if(m.rm() == 0) text = "Music?          Yes";
        else if (m.rm() == 1) text = "Music?          No";
        X();   
    }
    
    public void X() {
        stringLength = (text.length() + 2) * 10; //Find Text Length

        setImage(new GreenfootImage(stringLength, 16));
        
        //Create new Greenfoot Image (Text, size (font), Color, Background)
        GreenfootImage image = new GreenfootImage(text, 25, Color.RED, new Color(0, 0, 0, 0));
        
        image.setColor(Color.RED);
        
        setImage(image); 
    }
    
    public void act() {
        if (Greenfoot.mouseClicked(this) && text == "Music?          Yes") {
            text = "Music?          No"; //Text;
            X(); m.music(1);
        } else if (Greenfoot.mouseClicked(this) && text == "Music?          No") {
            text = "Music?          Yes"; //Text;
            X(); m.music(0);
        }
    }
}
