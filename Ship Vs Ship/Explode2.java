import greenfoot.*; 

/**
 * An Explosion
 *
 * @author Huu Vu
 */
public class Explode2 extends Mover {
    
    //Damage
    private int damage;

    // explode2 looses one life each act, and will disappear when life = 0
    private int life = 3;

    public Explode2(Vector speed, int rotation) {
        super(speed);
        setRotation(rotation);
        increaseSpeed(new Vector(rotation, 15));
        GreenfootSound sound = new GreenfootSound("Explosion.wav");
        sound.setVolume(70);
        sound.play();
    }

    public void act() {
        int r = Greenfoot.getRandomNumber(31);
        damage = r + 70;
        
        Rocket ro = (Rocket) getOneIntersectingObject(Rocket.class);
        Asteroid asteroid = (Asteroid) getOneIntersectingObject(Asteroid.class);
        Space space = (Space) getWorld();
        
        if (life <= 0) getWorld().removeObject(this);
            
        else {            
            if (ro != null) {                
                getWorld().removeObject(this);
                space.hit(damage);
                ro.hit(damage);                
            } else if (asteroid != null) {                
                life = 1;                
                if (life <= 0) {                    
                    life--;
                    getWorld().removeObject(this);                    
                }                
                asteroid.hit(damage);                
            } else life--;
        }
    }
}