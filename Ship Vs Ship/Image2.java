import greenfoot.*;

/**
 * Intruction
 * @author Huu Vu
 */
public class Image2 extends For2 {
    private GreenfootImage rocket = new GreenfootImage("rocket2.png");
    private GreenfootImage rocketWithThrust = new GreenfootImage("rocketWithThrust2.png");
    
    /**
     * Check whether there are any key pressed and react to them.
     */
    public void act() {
        if (Greenfoot.isKeyDown("up")) setImage(rocketWithThrust);            
        else if (Greenfoot.isKeyDown("down")) setImage(rocketWithThrust);            
        else setImage(rocket);
        if (Greenfoot.isKeyDown("left")) setRotation(getRotation() - 5);
        if (Greenfoot.isKeyDown("right")) setRotation(getRotation() + 5);
    }
}
