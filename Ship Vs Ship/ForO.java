import greenfoot.*;
import java.awt.*;

/**
 * Instruction for Other Items
 * @author Huu Vu
 */
public class ForO extends Instruction {
    private String text;
    private int stringLength;
    
    public ForO() {
        text = "Items will randomly appear on the screen.\n" +
               "Running over them will either help or hinder you.\n" +
               "The Asteroids will be affected by the items too if they run over it"; //Text
        
        stringLength = (text.length() + 2) * 10; //Find Text Length

        setImage(new GreenfootImage(stringLength, 16));
        
        //Create new Greenfoot Image (Text, size (font), Color, Background)
        GreenfootImage image = new GreenfootImage(text, 25, Color.RED, new Color(0, 0, 0, 0));
        
        image.setColor(Color.RED);
        
        setImage(image);   
    }
}