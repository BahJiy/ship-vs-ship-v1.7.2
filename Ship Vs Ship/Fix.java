import greenfoot.*;

/**
 * An Exposion that hurt Ships if they are hit
 *
 * Huu Vu
 */
public class Fix extends Items {
    //Damage
    private int fix;
    
    // A exp looses one life each act, and will disappear when life = 0
    private int life = 300;
    
    public void act() {
        Rocket ro = (Rocket) getOneIntersectingObject(Rocket.class);
        Rocket2 ro2 = (Rocket2) getOneIntersectingObject(Rocket2.class);
        Asteroid asteroid = (Asteroid) getOneIntersectingObject(Asteroid.class);
        Space space = (Space) getWorld();
        
        int r = Greenfoot.getRandomNumber(20);
        fix = r + 26;
        
        if (life <= 0) getWorld().removeObject(this);            
         else if (ro != null) {            
            getWorld().removeObject(this);
            space.hit(-fix);
            ro.hit(-fix);            
        } else if (ro2 != null) {            
            getWorld().removeObject(this);
            space.hit2(-fix);
            ro2.hit(-fix);            
        } else if (asteroid != null) {            
            asteroid.hit(-fix);
            getWorld().removeObject(this);
        } else life--;
    }
}